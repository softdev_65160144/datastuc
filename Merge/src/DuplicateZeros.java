public class DuplicateZeros {
    public static void main(String[] args){
        int[] numbers = {1,0,2,3,0,4,5,0};
        int[] keepnum = new int[numbers.length];
        int position = 0;
        for(int i=0; i<numbers.length; i++){
            if(numbers[i]==0&&position<numbers.length){
                keepnum[position++] = numbers[i];
                keepnum[position++] = 0;
            }else{
                keepnum[position++] = numbers[i];
            }
            if (position==keepnum.length){
                break;
            }
        }
        for(int i=0; i<numbers.length; i++){
            System.out.print(keepnum[i]+" ");
        }
}
}
